import React , { useState, useEffect } from 'react';
import axios from 'axios';
import Posts from "./posts"
import Pagination from "./Pagination"

function App() {
	const [posts, setPosts] = useState([])
	const [currentPage, setCurrentPage] = useState(1)
	const postsPerPage =10

	// useEffect to call the api for gettting data using axios
	useEffect(()=>{
		// async required since its an api call and does takes some to get the data
		const fetchPosts =async ()=>{	
			const response = await axios.get("https://jsonplaceholder.typicode.com/posts")
			setPosts(response.data)
		}
		fetchPosts()
	},[])

	//finding the extreme post index of a single page
	const indexOfLastPost = currentPage*postsPerPage
	const indexOfFirstPost = indexOfLastPost - postsPerPage
	const currentPosts = posts.slice(indexOfFirstPost, indexOfLastPost)

	const totalNoOfPages = Math.ceil(posts.length/postsPerPage)

	// callback from the Pagination component
	const changePageNumber = (number) =>{
		setCurrentPage(number)
	}

  return (
    <div className="App">
		<Posts  currentPosts={currentPosts}/>
		<br/>
		<hr/>
		<Pagination totalNoOfPages={totalNoOfPages} changePageNumber={changePageNumber}/>
    </div>
  );
}

export default App;
