import React from 'react'

// listing all the posts within the range (indexOfFirstPost, indexOfLastPost)
const Posts = ({currentPosts}) => {
	return (
		<div>
			{currentPosts.map(post=>{
			return(
				<li key = {post.id}>{post.title}</li>
			)
		})}
		</div>
	)
}

export default Posts
