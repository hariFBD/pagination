import React from 'react'

const Pagination = ({totalNoOfPages, changePageNumber}) => {
	const PageNumbers =[]
	// pagenumbers to show a pagination is stored as a array called PageNumbers
	for(let i=1;i<=totalNoOfPages;i++){
		PageNumbers.push(i)
	}
	return (
		<div>
			Pages : -
			{PageNumbers.map(number=>{
				return (
					// onClick there cmponents sends the callback with the clicked page number as a param
					<button onClick ={()=>changePageNumber(number)} key={number}>{number} </button>
				)
			})}
		</div>
	)
}

export default Pagination
